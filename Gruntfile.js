'use strict';

module.exports = function (grunt) {
    grunt.initConfig({
        connect: {
            options: {
                port: 9000,
                // Change this to '0.0.0.0' to access the server from outside.
                hostname: '0.0.0.0',
                livereload: 35729
            },
            livereload: {
                options: {
                    open: true,
                    base: [
                    ''
                    ]
                }
            }
        }
    });

    grunt.registerTask('default',['connect:livereload:keepalive']);
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-connect');
};
