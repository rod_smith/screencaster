Screencaster Proof of Concept
===

This is a simplified proof of concept to allow for creating screencasts of the screen with Javascript.  Currently is just using RecorderJS, but will need to include some sort of Flash-based fallback for the "lesser" browsers.

The player.html file is the one that actually plays back the mouse movements.  It is the one that PhantomJS would be calling.

The idea for the workflow is something like this code in PhantomJS:

	var page = require('webpage').create();
	page.viewportSize = { width: 640, height: 480 }; // or whatever makes sense, probably a variable based on the user's viewport

	page.open('YOUR PAGE URL', function () {
	  var counter = 0;
	  setInterval(function() {
	    counter++;
	    page.render('recording-image' + counter + '.png', { format: "png" });
	  }, YOUR RECORDING INTERVAL);
	});

Then you'd have a folder of images to pass into FFMpeg with the Audio stream.  So in FFMPEG, something like:

	ffmpeg -i recording-image%03d.png -i audio.wav -c:v libx264 -c:a aac -strict experimental -b:a 192k -shortest out.mp4

There might need to be some tweaking to get it where you want, but that should get you 90% of the way there.
