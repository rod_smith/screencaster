/**
* SCREENCASTER
*
* Core logic for the screencasting
* front-end
*/
'use strict';

(function() {
    // Actual record button element
    var recordButton = document.querySelector('.record'),
        // Done button selector
        doneButton = document.getElementById('done'),
        // Frames to capture mouse movement into (~24fps)
        mouseFrames = [],
        // FPS to record
        interval = (1000/24),
        // Bool to see if recording is going on
        isRecording = false,
        // Coordinates for mouse (X and Y)
        mouseCoords = { x: 0, y: 0 },
        // Coordinates for scrolling (X and Y)
        scrollCoords = { x: window.pageXOffset, y: window.pageYOffset },
        // Actual size of the browser window
        windowSize = { width: window.innerWidth, height: window.innerHeight },
        // <body> tag
        body = document.body,
        // <html> tag
        html = document.documentElement,
        // AJAX for later
        xmlhttp, theXhr,
        // Audio Context for Mic access
        audio_context,
        // RecorderJS Instance
        recorder;

    // Listeners
    recordButton.addEventListener('click', function(event) {
        event.preventDefault();
        this.classList.toggle('active');
        isRecording = true;

        recorder.record();
    });

    doneButton.addEventListener('click', function(event) {
        event.preventDefault();
        isRecording = false;

        // Download WAV file (for now)
        recorder.stop();
        recorder.exportWAV(function(blob) {
            Recorder.forceDownload(blob);
        });

        // Ajax over content
        xmlhttp = new XMLHttpRequest({mozSystem: true});
        var url = "http://localhost:8888/projects/screencaster-ajax/";
        var params = "content=" + encodeURIComponent(JSON.stringify(mouseFrames));
        xmlhttp.open("POST", url, true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
        xmlhttp.send(params);
    });

    window.onmousemove = function(event) {
        if( event.pageX || event.pageY ) {
            mouseCoords.x = event.pageX;
            mouseCoords.y = event.pageY;
        }
    };

    window.onresize = function(event) {
        windowSize.width = window.innerWidth;
        windowSize.height = window.innerHeight;
    };

    window.onscroll = function(event) {
        if( window.pageXOffset || window.pageYOffset ) {
            scrollCoords.x = window.pageXOffset;
            scrollCoords.y = window.pageYOffset;
        } else {
            scrollCoords.x = document.documentElement.scrollLeft || document.body.scrollLeft;
            scrollCoords.y = document.documentElement.scrollTop || document.body.scrollTop;
        }
    };

    setInterval(function() {
        if( isRecording ) {
            mouseFrames.push([mouseCoords.x, mouseCoords.y, scrollCoords.x, scrollCoords.y, windowSize.width, windowSize.height]);
        }
    }, interval);

    /**
     * AUDIO
     */
    try {
      // shim
      window.AudioContext = window.AudioContext || window.webkitAudioContext;
      navigator.getUserMedia = ( navigator.getUserMedia ||
                               navigator.webkitGetUserMedia ||
                               navigator.mozGetUserMedia ||
                               navigator.msGetUserMedia);
      window.URL = window.URL || window.webkitURL || window.mozURL;

      audio_context = new AudioContext;
    } catch (e) {
      alert('No web audio support in this browser!');
    }

    navigator.getUserMedia({audio: true}, startUserMedia, function(e) {});

    function startUserMedia(stream) {
        var input = audio_context.createMediaStreamSource(stream);
        input.connect(audio_context.destination);
        recorder = new Recorder(input);
    }

    window.mouseFrames = mouseFrames;
})();
